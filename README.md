

## Project setup

Dependencies:
- docker
- php 7.2
- composer
- nodejs

Run to build the containers:
```
docker-compose build
docker-compose run composer composer install
docker-compose run node npm install
```

### Run
```
docker-compose up -d
```

### Live version:

Deploye to Google Cloud App Engine
```
https://frontend-dot-stable-smithy-291111.ew.r.appspot.com/ => frontend
https://apigility-dot-stable-smithy-291111.ew.r.appspot.com/apigility/swagger => swagger interface for backend
https://apigility-dot-stable-smithy-291111.ew.r.appspot.com/numbers_guess => endpoint
```

### Data Structure:
```
By calling the POST method to the numbers_guess endpoint this JSON object as request body:

{
  "name": "Cool Name",
  "numbers": { "initialNumbers ": [1, 8, 27, 64] } 
}


should get a reply with:

{ 
    "id": "1",
    "name": "Cool Name",
    "numbers": "{ "result": 125, "initialNumbers": [1, 8, 27, 64] },
    "_links": { 
        "self": { "href": "http://localhost:8080/numbers_guess/1" }
    }
}
```