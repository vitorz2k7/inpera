use inpera;
CREATE TABLE numbers_guess (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `numbers` JSON NULL,
  PRIMARY KEY (`id`));
