<?php
return [
    'router' => [
        'routes' => [
            'inpera.rest.numbers-guess' => [
                'may_terminate' => true,
                'type' => 'Segment',
                'options' => [
                    'route' => '/numbers_guess',
                    'defaults' => [
                        'controller' => 'inpera\\V1\\Rest\\NumbersGuess\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'inpera.rest.numbers-guess',
        ],
    ],
    'zf-rest' => [
        'inpera\\V1\\Rest\\NumbersGuess\\Controller' => [
            'listener' => \inpera\V1\Rest\NumbersGuess\NumbersGuessResource::class,
            'route_name' => 'inpera.rest.numbers-guess',
            'route_identifier_name' => 'numbers_guess_id',
            'collection_name' => 'numbers_guess',
            'entity_http_methods' => [],
            'collection_http_methods' => [
                0 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \inpera\V1\Rest\NumbersGuess\NumbersGuessEntity::class,
            'collection_class' => \inpera\V1\Rest\NumbersGuess\NumbersGuessCollection::class,
            'service_name' => 'numbers_guess',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'inpera\\V1\\Rest\\NumbersGuess\\Controller' => 'HalJson',
        ],
        'accept_whitelist' => [
            'inpera\\V1\\Rest\\NumbersGuess\\Controller' => [
                0 => 'application/vnd.inpera.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
                3 => 'text/html',
            ],
        ],
        'content_type_whitelist' => [
            'inpera\\V1\\Rest\\NumbersGuess\\Controller' => [
                0 => 'application/vnd.inpera.v1+json',
                1 => 'application/json',
                2 => 'text/html',
            ],
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            \inpera\V1\Rest\NumbersGuess\NumbersGuessEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'inpera.rest.numbers-guess',
                'route_identifier_name' => 'numbers_guess_id',
                'hydrator' => \Zend\Hydrator\ArraySerializableHydrator::class,
            ],
            \inpera\V1\Rest\NumbersGuess\NumbersGuessCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'inpera.rest.numbers-guess',
                'route_identifier_name' => 'numbers_guess_id',
                'is_collection' => true,
            ],
        ],
    ],
    'zf-apigility' => [
        'db-connected' => [
            \inpera\V1\Rest\NumbersGuess\NumbersGuessResource::class => [
                'adapter_name' => 'mysql_adapter',
                'table_name' => 'numbers_guess',
                'hydrator_name' => \Zend\Hydrator\ArraySerializableHydrator::class,
                'controller_service_name' => 'inpera\\V1\\Rest\\NumbersGuess\\Controller',
                'entity_identifier_name' => 'id',
                'table_service' => 'inpera\\V1\\Rest\\NumbersGuess\\NumbersGuessResource\\Table',
            ],
        ],
    ],
    'zf-content-validation' => [
        'inpera\\V1\\Rest\\NumbersGuess\\Controller' => [
            'input_filter' => 'inpera\\V1\\Rest\\NumbersGuess\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'inpera\\V1\\Rest\\NumbersGuess\\Validator' => [
            0 => [
                'name' => 'name',
                'required' => true,
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StringTrim::class,
                    ],
                    1 => [
                        'name' => \Zend\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => '45',
                        ],
                    ],
                ],
            ],
            1 => [
                'name' => 'numbers',
                'required' => true,
                'filters' => [],
                'validators' => [],
            ],
        ],
    ],
    'service_manager' => [
        'invokables' => [
            'ZF\\Rest\\RestParametersListener' => \ZF\Rest\Listener\RestParametersListener::class,
        ],
        'factories' => [
            \inpera\V1\Rest\NumbersGuess\NumbersGuessResource::class => \inpera\V1\Rest\NumbersGuess\NumbersGuessResourceFactory::class,
            'ZF\\Rest\\OptionsListener' => \ZF\Rest\Factory\OptionsListenerFactory::class,
        ],
        'abstract_factories' => [
            0 => \ZF\Apigility\DbConnectedResourceAbstractFactory::class,
            1 => \ZF\Apigility\TableGatewayAbstractFactory::class,
        ],
    ],
    'controllers' => [
        'abstract_factories' => [
            0 => \ZF\Rest\Factory\RestControllerFactory::class,
        ],
    ],
    'view_manager' => [
        'display_exceptions' => true,
    ],
];
