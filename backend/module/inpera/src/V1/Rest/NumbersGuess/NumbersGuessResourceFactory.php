<?php
namespace inpera\V1\Rest\NumbersGuess;

use Zend\Db\TableGateway\TableGateway;
use Zend\ServiceManager\ServiceLocatorInterface;

class NumbersGuessResourceFactory
{
    /**
     * @param $services ServiceLocatorInterface
     * @return NumbersGuessResource
     */
    public function __invoke($services)
    {
        $guesserService = new GuesserService();

        /** @var TableGateway $tableGateway */
        $tableGateway = $services->get('inpera\\V1\\Rest\\NumbersGuess\\NumbersGuessResource\\Table');

        return new NumbersGuessResource($guesserService, $tableGateway);
    }
}
