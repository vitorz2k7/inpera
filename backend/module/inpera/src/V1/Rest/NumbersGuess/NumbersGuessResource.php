<?php
namespace inpera\V1\Rest\NumbersGuess;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\TableGateway\TableGatewayInterface;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class NumbersGuessResource extends AbstractResourceListener
{
    /**
     * @var GuesserService
     */
    private $guesserService;

    /**
     * @var AbstractTableGateway
     */
    private $tableGateway;


    /**
     * NumbersGuessResource constructor.
     * @param $guesserService GuesserService
     * @param $tableGateway TableGatewayInterface
     * @param $config
     */
    public function __construct($guesserService, $tableGateway)
    {
        $this->guesserService = $guesserService;
        $this->tableGateway = $tableGateway;
    }

    /**
     * Create a resource
     *
     * @param mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        try {
            $numbersField = json_decode($data->numbers);

            $numberArray = $numbersField->initialNumbers;
            $this->guesserService->setNumbers($numberArray);
            $result = $this->guesserService->guess();
            $numbersField->result = (int) $result;

            $entityToSave = [
                'name' => $data->name,
                'numbers' => json_encode($numbersField)
            ];

            $this->tableGateway->insert($entityToSave);
            $insertedId = $this->tableGateway->getLastInsertValue();

            /** @var ResultSet $insertedData */
            $insertedData = $this->tableGateway->select('id = '.$insertedId);

            return $insertedData->current();

        } catch (\Exception $exception) {

            return new ApiProblem(500, $exception->getMessage());
        }
    }
}
