<?php
namespace inpera\V1\Rest\NumbersGuess;


use Exception;
use MathPHP\NumericalAnalysis\Interpolation\LagrangePolynomial;

class GuesserService
{
    /**
     * @var array
     */
    private $points = [];

    /**
     * @param $chosenNumbers
     */
    public function setNumbers($chosenNumbers) {
        $numberMapper = function($x, $y) {
            return [$x, $y];
        };

        $this->points = array_map($numberMapper, [1, 2, 3, 4], $chosenNumbers);
    }

    /**
     * @return int
     * @throws Exception
     */
    public function guess() {
        try {
            $polynomial = LagrangePolynomial::interpolate($this->points);
            return $polynomial(5);
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
