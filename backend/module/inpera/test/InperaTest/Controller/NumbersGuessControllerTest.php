<?php
namespace InperaTest\Controller;

use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class NumbersGuessControllerTest extends AbstractHttpControllerTestCase
{
    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        $configOverrides = [
            'module_listener_options' => [
                'config_cache_enabled' => false,
            ],
        ];

        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../../config/application.config.php',
            $configOverrides
        ));

        parent::setUp();
    }

    public function testPostActionCanBeAccessed()
    {
        $toEncode = [
            'name' => 'my name is test',
            'numbers' => json_encode(
                ['initialNumbers' => [2, 4, 6, 8]]
            )
        ];
        $body = json_encode($toEncode);
        $this->dispatch('/numbers_guess', 'POST', [ 'body' => $body ]);
        $this->assertResponseStatusCode(201);
        $this->assertModuleName('inpera');
        $this->assertControllerName('inpera\\V1\\Rest\\NumbersGuess\\Controller'); // as specified in router's controller name alias
        $this->assertControllerClass('NumbersGuessController');
        $this->assertMatchedRouteName('/numbers_guess');
    }

    public function testInvalidHttpMethodCall()
    {
        $this->dispatch('/numbers_guess', 'GET');
        $this->assertResponseStatusCode(405);
    }

    public function testInvalidRouteDoesNotCrash()
    {
        $this->dispatch('/invalid/route', 'GET');
        $this->assertResponseStatusCode(404);
    }
}
