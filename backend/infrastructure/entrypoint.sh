#!/bin/bash

set -xe

cp /app/infrastructure/php-fpm.conf /usr/local/etc/php-fpm.conf
cp /app/infrastructure/supervisord.conf /etc/supervisor/supervisord.conf
cp /app/infrastructure/nginx.conf /etc/nginx/nginx.conf
cp /app/infrastructure/inpera.conf /etc/nginx/conf.d/inpera.conf
cp /app/infrastructure/mime.types /etc/nginx/mime.types


# Configure memcached based session.
if [ -n "${MEMCACHE_PORT_11211_TCP_ADDR}" ] && [ -n "${MEMCACHE_PORT_11211_TCP_PORT}" ]; then
    cat <<EOF > ${PHP_DIR}/lib/conf.d/memcached-session.ini
session.save_handler=memcached
session.save_path="${MEMCACHE_PORT_11211_TCP_ADDR}:${MEMCACHE_PORT_11211_TCP_PORT}"
EOF
fi

if [ -f "${APP_DIR}/composer.json" ]; then
    # run the composer scripts for post-deploy
    if su www-data -c "php /usr/local/bin/composer --no-ansi run-script -l" \
        | grep -q "post-deploy-cmd"; then
        su www-data -c \
            "php /usr/local/bin/composer run-script post-deploy-cmd \
            --no-ansi \
            --no-interaction" \
            || (echo 'Failed to execute post-deploy-cmd'; exit 1)
    fi
fi

exec "$@"